# CyberT33N Scrap Bot - Core Engine
This is the core engine of the CyberT33N Scrap Bot.
<br />
<br />
Database: **MongoDB**
<br />
Webdriver: **Puppeteer**

<br />
<br />
★ 𝐎𝐒 ★

Warranty and Support for:<br />
• **Windows 7-10 (64-bit)**<br />
• **Windows Server 2012 R2 (64-bit)**<br />

• **MAC El Capitan (64-bit)**<br />
• **MAC Sierra (64-bit)**<br />
• **MAC High Sierra (64-bit)**<br />

• **CentOS (64-bit)**<br />
• **Linux Mint (64-bit)**<br />
• **Fedora (64-bit)**<br />
• **Ubuntu (64-bit)**<br /><br />


Normally they will work with all other versions/architectures aswell!
<br /><br />
You can easily use the .iso files from the OS above with Virtual Machines, Server Side or at some OS Cloud Service. It works aswell at your own Computer at Home! :)

<br />
<br />

 _____________________________________________________
 _____________________________________________________


<br />
<br />

# *DOCUMENTATION*
Full install guides and all needed informations can be found at the documentation at this folder:
```bash
./GUIDES/DOCUMENTATION/index.html
```

<br />
<br />
Please notice that the documentation was build long time ago and is outdated and is not designed properly. It was created when I had no experience at web design. Even when the core informations are still correct some informations might be not be up 2 date anymore.
<br />
<br />
However I used the documentation back in the days on upwork but since I do not work there anymore I never updated the documentation properly to puppeteer and MongoDB. The documentation will maybe re-designed in the future but I dont think so unless there will be more customers for my bots.. It would be just a time waste for my side to create a new documentation at this point. I hope you understand.
<br />
<br />
However if you know how Node.js, Puppeteer & MongoDB works there is literally no need to read the documentation.

<br />
<br />

 _____________________________________________________
 _____________________________________________________


<br />
<br />


## License  
The CyberT33N Core engine is **not** MIT and is copyright protected by CyberT33N Software. If any scrap bot from me is MIT you can use this core engine and the scrap script itself of course for FREE for personal or commercial usage. However the re-creating/re-publishing/selling/.. or any other form of modification of this core engine is not allowed.

If the main scrap script itself is MIT you can use the script aswell without this Core engine. You can create your own Core engine if you want.
